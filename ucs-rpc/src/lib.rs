use leptos::{tracing::info, *};

pub mod auth;
use auth::User;

cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")]
    {
        use auth::auth;
        use leptos_axum::ResponseOptions;
        use sqlx::SqlitePool;
        fn pool(cx: Scope) -> Result<SqlitePool, ServerFnError> {
            use_context::<SqlitePool>(cx)
                .ok_or_else(|| ServerFnError::ServerError("Response Options missing".to_string()))
        }

        fn response_options(cx: Scope) -> Result<ResponseOptions, ServerFnError> {
            use_context::<ResponseOptions>(cx)
                .ok_or_else(|| ServerFnError::ServerError("Response Options missing".to_string()))
        }
    }
}

#[server(GetUser, "/api")]
pub async fn get_user(cx: Scope) -> Result<Option<User>, ServerFnError> {
    let auth = auth(cx)?;

    Ok(auth.current_user)
}

#[server(Login, "/api")]
pub async fn login(
    cx: Scope,
    username: String,
    password: String,
    remember: Option<String>,
) -> Result<(), ServerFnError> {
    let pool = pool(cx)?;
    let auth = auth(cx)?;

    match User::login(&username, &password, &pool).await {
        Ok(Some(id)) => {
            info!("User Logged In!");
            auth.login_user(id);
            auth.remember_user(remember.is_some());

            leptos_axum::redirect(cx, "/");
            Ok(())
        }
        Ok(None) => {
            let response = response_options(cx)?;
            response.set_status(axum::http::StatusCode::UNAUTHORIZED);
            Ok(())
        }
        Err(e) => Err(ServerFnError::ServerError(e)),
    }
}

#[server(Logout, "/api")]
pub async fn logout(cx: Scope) -> Result<(), ServerFnError> {
    let auth = auth(cx)?;
    auth.logout_user();
    leptos_axum::redirect(cx, "/");
    Ok(())
}

#[server(Register, "/api")]
pub async fn register(
    cx: Scope,
    username: String,
    password: String,
    invite_code: String,
    remember: Option<String>,
) -> Result<Option<String>, ServerFnError> {
    let response = response_options(cx)?;
    let root_invite_code = std::env::var("ROOT_INVITE_CODE").ok();
    let pool = pool(cx)?;
    let auth = auth(cx)?;

    if password.is_empty() {
        response.set_status(axum::http::StatusCode::BAD_REQUEST);
        return Ok(Some("Password is Required!".to_string()));
    }

    if username == "root" {
        if let Some(root_code) = root_invite_code {
            if invite_code != root_code {
                response.set_status(axum::http::StatusCode::UNAUTHORIZED);
                return Ok(None);
            }
        }
    } else {
        todo!("Check invite code")
    }

    match User::create(&username, &password, &pool).await {
        Ok(Some(id)) => {
            auth.login_user(id);
            auth.remember_user(remember.is_some());
            leptos_axum::redirect(cx, "/");
            Ok(None)
        }
        Ok(None) => {
            response.set_status(axum::http::StatusCode::UNAUTHORIZED);
            Ok(None)
        }
        Err(e) => Err(ServerFnError::ServerError(e)),
    }
}
