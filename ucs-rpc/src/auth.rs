use cfg_if::cfg_if;

use leptos::{error, use_context, Scope, ServerFnError};
use serde::{Deserialize, Serialize};

cfg_if! {
    if #[cfg(feature = "ssr")]
    {
        use axum_session::SessionSqlitePool;
        use axum_session_auth::Authentication;
        use sqlx::{SqlitePool};
        use scrypt::{Scrypt, password_hash::{rand_core::OsRng,  PasswordHasher, SaltString}};

        pub type AuthSession = axum_session_auth::AuthSession<User, i64, SessionSqlitePool, SqlitePool>;

        pub fn auth(cx: Scope) -> Result<AuthSession, ServerFnError> {
            use_context::<AuthSession>(cx).ok_or_else(|| {
                error!("Auth Session missing");
                ServerFnError::ServerError("Auth Session missing".to_string())
            })
        }

        #[derive(sqlx::FromRow)]
        struct SqlUser {
            pub id: i64,
            pub username: String,
            pub password: String,
            pub salt: String,
        }

        impl SqlUser{
            async fn get(id: i64, pool: &SqlitePool) -> Option<Self> {
                sqlx::query_as::<_, SqlUser>("SELECT * FROM users WHERE id = ?")
                    .bind(id)
                    .fetch_one(pool)
                    .await
                    .ok()
            }



            pub async fn get_by_name(username: &str, pool: &SqlitePool) -> Option<Self>{
sqlx::query_as::<_, SqlUser>("SELECT * FROM users WHERE username = ?")
                    .bind(username)
                    .fetch_one(pool)
                    .await
                    .ok()        }
            }

        impl User {
            async fn get(id: i64, pool: &SqlitePool) -> Option<Self> {
                let sqluser = SqlUser::get(id, pool)
                    .await?;

                Some(User {
                    id: sqluser.id,
                    username: sqluser.username,
                })
            }

            pub async fn get_by_name(username: &str, pool: &SqlitePool) -> Option<Self>{

                let sqluser = SqlUser::get_by_name(username, pool).await?;

                Some(User {
                    id: sqluser.id,
                    username: sqluser.username,
                })
            }

            pub async fn login(username: &str, password: &str, pool: &SqlitePool) -> Result<Option<i64>, String> {
                let Some(sqluser) = SqlUser::get_by_name(username, pool).await else {
                    return Ok(None);
                };

                let salt = SaltString::from_b64(&sqluser.salt).map_err(|e| e.to_string())?;
                let password_hash = Scrypt.hash_password(password.as_bytes(), &salt).map_err(|e| e.to_string())?.serialize();

                let password_hash = password_hash.as_str();
                if password_hash == sqluser.password
                {
                    Ok(Some(sqluser.id))
                }
                else {
                    Ok(None)
                }
            }

            pub async fn create(username: &str, password:&str, pool: &SqlitePool) -> Result<Option<i64>, String>{

                if Self::get_by_name(username, pool).await.is_some(){
                    return Ok(None);
                }


                let salt_string = SaltString::generate(&mut OsRng);
                let salt = salt_string.as_str();
                let password_hash = Scrypt.hash_password(password.as_bytes(), &salt_string).map_err(|e| e.to_string())?.serialize();

                let password_hash = password_hash.as_str();



                sqlx::query("INSERT INTO users (username, password, salt) VALUES (?, ?, ?)")
                    .bind(username)
                    .bind(password_hash)
                    .bind(salt)
                    .execute(pool)
                    .await.map_err(|e| e.to_string())?;

                Ok(Self::get_by_name(username, pool).await.map(|u| u.id))
            }
        }

        #[async_trait::async_trait]
        impl Authentication<User, i64, SqlitePool> for User {
            async fn load_user(userid: i64, pool: Option<&SqlitePool>) -> Result<User, anyhow::Error> {
                let pool = pool.unwrap();

                User::get(userid, pool).await
                 .ok_or_else(|| anyhow::anyhow!("Cannot get user"))
            }

            fn is_authenticated(&self) -> bool {
                self.id != -1
            }

            fn is_active(&self) -> bool {
                self.is_authenticated()
            }

            fn is_anonymous(&self) -> bool {
                self.id == -1
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub id: i64,
    pub username: String,
}

impl Default for User {
    fn default() -> Self {
        Self {
            id: -1,
            username: String::new(),
        }
    }
}
