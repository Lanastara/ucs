use std::collections::{BTreeMap, HashMap, HashSet};

use evalexpr::{ContextWithMutableVariables, IterateVariablesContext, Node, Value};
use petgraph::prelude::GraphMap;
use ucs_data::{Character, System};

fn get_graph_index(
    name: &String,
    index: &mut usize,

    lookup: &mut HashMap<String, usize>,
    rev_lookup: &mut Vec<String>,
    graph: &mut GraphMap<usize, (), petgraph::Directed>,
    attribute_nodes: &mut Vec<(Option<Node>, BTreeMap<u8, Vec<(Condition, Node)>>)>,
) -> usize {
    match lookup.get(name) {
        Some(i) => *i,
        None => {
            let i = *index;

            lookup.insert(name.clone(), *index);
            rev_lookup.push(name.clone());
            graph.add_node(*index);
            attribute_nodes.push((None, BTreeMap::new()));
            *index += 1;

            i
        }
    }
}

fn insert_node(
    attribute_nodes: &mut Vec<(Option<Node>, BTreeMap<u8, Vec<(Condition, Node)>>)>,
    index: usize,
    details: Option<(u8, Condition)>,
    node: Node,
) {
    let (default, map) = attribute_nodes.get_mut(index).unwrap();

    if let Some((level, condition)) = details {
        match map.get_mut(&level) {
            Some(v) => {
                v.push((condition, node));
            }
            None => {
                let v = vec![(condition, node)];

                map.insert(level, v);
            }
        }
    } else {
        *default = Some(node);
    }
}

#[derive(Debug, Clone)]
enum Condition {
    Condition(String, Node),
    Feature(String),
}

struct FlatSystem {
    logic: Vec<(String, (Option<Node>, Vec<(Condition, Node)>))>,
    features: HashMap<String, Vec<String>>,
}

fn flatten_system(system: System) -> Result<FlatSystem, ()> {
    let mut lookup = HashMap::new();
    let mut graph = GraphMap::new();
    let mut rev_lookup = Vec::new();

    let mut attribute_nodes = Vec::with_capacity(system.attributes.len());
    let mut features = HashMap::new();

    let mut index = 0;

    for (name, attribute) in system.attributes {
        let node_index = get_graph_index(
            &name,
            &mut index,
            &mut lookup,
            &mut rev_lookup,
            &mut graph,
            &mut attribute_nodes,
        );

        if let Some(formula) = attribute.formula {
            if formula.iter_write_variable_identifiers().next().is_some() {
                return Err(());
            }

            for att in formula.iter_read_variable_identifiers() {
                let att = att.to_string();
                let i = get_graph_index(
                    &att,
                    &mut index,
                    &mut lookup,
                    &mut rev_lookup,
                    &mut graph,
                    &mut attribute_nodes,
                );

                graph.add_edge(i, node_index, ());
            }
            insert_node(&mut attribute_nodes, node_index, None, formula);
        }
    }

    for (f_name, feature) in system.features {
        features.insert(f_name.clone(), feature.additional_features);
        let condition = if let Some(condition) = feature.condition {
            if condition.iter_write_variable_identifiers().next().is_some() {
                return Err(());
            }

            Condition::Condition(f_name, condition)
        } else {
            Condition::Feature(f_name)
        };

        for effect in feature.effects {
            let Some(name) = effect.formula.iter_write_variable_identifiers().next()else {
                return Err(());
            };

            let name = name.to_string();

            let node_index = get_graph_index(
                &name,
                &mut index,
                &mut lookup,
                &mut rev_lookup,
                &mut graph,
                &mut attribute_nodes,
            );

            for att in effect.formula.iter_read_variable_identifiers() {
                let att = att.to_string();
                let i = get_graph_index(
                    &att,
                    &mut index,
                    &mut lookup,
                    &mut rev_lookup,
                    &mut graph,
                    &mut attribute_nodes,
                );

                graph.add_edge(i, node_index, ());
            }

            insert_node(
                &mut attribute_nodes,
                node_index,
                Some((effect.priority, condition.clone())),
                effect.formula,
            )
        }
    }

    let v = petgraph::algo::toposort(&graph, None).unwrap();

    let mut logic = Vec::with_capacity(v.len());
    for index in v {
        let (default, formulas) = attribute_nodes.get_mut(index).unwrap();

        let x: Vec<_> = formulas
            .keys()
            .rev()
            .filter_map(|k| formulas.get(k))
            .flatten()
            .cloned()
            .collect();

        let name = rev_lookup.get(index).unwrap().clone();

        logic.push((name, (default.clone(), x)));
    }

    Ok(FlatSystem { logic, features })
}

fn fill_features(
    feature: &String,
    hierachy: &HashMap<String, Vec<String>>,
    features: &mut HashSet<String>,
) {
    if let Some(list) = hierachy.get(feature) {
        for feature in list {
            fill_features(feature, hierachy, features);
        }
    }
    if !features.contains(feature) {
        features.insert(feature.clone());
    }
}

fn fill_character(system: &FlatSystem, character: &Character) -> Result<Character, ()> {
    let mut features = HashSet::new();
    for (_field, feature) in &character.features {
        fill_features(&feature, &system.features, &mut features);
    }

    dbg!(&features);

    let mut context = evalexpr::HashMapContext::new();

    for (name, (default, rules)) in &system.logic {
        if let Some(v) = character.attributes.get(name) {
            context.set_value(name.clone(), Value::Int(*v)).unwrap();
        } else {
            match default {
                Some(default) => {
                    let v = default.eval_int_with_context(&context).unwrap();
                    context.set_value(name.clone(), Value::Int(v)).unwrap();
                }
                None => {
                    context.set_value(name.clone(), Value::Int(0)).unwrap();
                }
            }

            for (condition, rule) in rules {
                if match condition {
                    Condition::Condition(f, c) => {
                        features.contains(f) && c.eval_boolean_with_context(&context).unwrap()
                    }
                    Condition::Feature(f) => features.contains(f),
                } {
                    rule.eval_with_context_mut(&mut context).unwrap();
                }
            }
        }
    }

    let mut attributes = BTreeMap::new();
    for (name, value) in context.iter_variables() {
        attributes.insert(name, value.as_int().unwrap());
    }

    Ok(Character {
        name: character.name.clone(),
        attributes,
        features: character.features.clone(),
        texts: character.texts.clone(),
    })
}

#[cfg(test)]
mod test {
    use ucs_data::Character;

    use super::*;

    const CHARACTER: &str = include_str!("../../data/char.json");
    const SYSTEM: &str = include_str!("../../data/system.json");

    #[test]
    fn build_system_graph() {
        let system: System = serde_json::from_str(SYSTEM).unwrap();

        let _g = flatten_system(system).unwrap();
    }

    #[test]
    fn text_fill_character() {
        let system: System = serde_json::from_str(SYSTEM).unwrap();
        let c: Character = serde_json::from_str(CHARACTER).unwrap();

        let g = flatten_system(system).unwrap();

        let cha = fill_character(&g, &c).unwrap();

        panic!("{:?}", cha);
    }
}
