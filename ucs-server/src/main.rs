use std::net::SocketAddr;

use axum::{
    body::Body as AxumBody,
    extract::{Path, RawQuery, State},
    response::IntoResponse,
    routing::{get, post},
    Router,
};
use axum_session::*;
use axum_session_auth::*;
use fallback::file_and_error_handler;
use http::{HeaderMap, Request};
use leptos::provide_context;
use leptos_axum::handle_server_fns_with_context;
use sqlx::{sqlite::SqlitePoolOptions, Pool, Sqlite, SqlitePool};
use tower_http::trace::TraceLayer;
use tracing_subscriber::{filter::LevelFilter, EnvFilter, FmtSubscriber};
use ucs_rpc::auth::User;

mod fallback;

async fn test(session: ucs_rpc::auth::AuthSession) -> String {
    session
        .current_user
        .map(|user| user.username)
        .unwrap_or_default()
}

async fn server_fn_handler(
    State(pool): State<Pool<Sqlite>>,
    auth_session: ucs_rpc::auth::AuthSession,
    path: Path<String>,
    headers: HeaderMap,
    raw_query: RawQuery,
    request: Request<AxumBody>,
) -> impl IntoResponse {
    handle_server_fns_with_context(
        path,
        headers,
        raw_query,
        move |cx| {
            provide_context(cx, auth_session.clone());
            provide_context(cx, pool.clone());
        },
        request,
    )
    .await
}

#[tokio::main]
async fn main() {
    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::DEBUG.into())
        .from_env()
        .unwrap();
    let subscriber = FmtSubscriber::builder().with_env_filter(filter).finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    let pool = SqlitePoolOptions::new()
        .connect("sqlite:ucs.db")
        .await
        .unwrap();

    let session_config = SessionConfig::default().with_table_name("sessions");
    let auth_config = AuthConfig::<i64>::default().with_anonymous_user_id(Some(-1));
    let session_store =
        SessionStore::<SessionSqlitePool>::new(Some(pool.clone().into()), session_config)
            .await
            .unwrap();
    session_store.initiate().await.unwrap();

    sqlx::migrate!().run(&pool).await.unwrap();

    let app = Router::new()
        .route(
            "/api/*fn_name",
            post(server_fn_handler).get(server_fn_handler),
        )
        .route("/test", get(test))
        .fallback(file_and_error_handler)
        .layer(TraceLayer::new_for_http())
        .layer(
            AuthSessionLayer::<User, i64, SessionSqlitePool, SqlitePool>::new(Some(pool.clone()))
                .with_config(auth_config),
        )
        .layer(SessionLayer::new(session_store))
        .with_state(pool);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
