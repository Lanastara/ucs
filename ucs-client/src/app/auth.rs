use leptos::*;
use leptos_router::ActionForm;

#[component]
fn Login(cx: Scope, login: Action<ucs_rpc::Login, Result<(), ServerFnError>>) -> impl IntoView {
    view! {
        cx,
        <div>
            <h2 >Login</h2>
            <ActionForm class="form-control" action=login>
                <label class="label">
                    <span class="label-text">Username</span>
                </label>
                <input type="text" class="input input-bordered" name="username" />
                <label class="label">
                    <span class="label-text">Password</span>
                </label>
                <input type="password" class="input input-bordered" name="password" />
                <label class="label cursor-pointer">
                    <span class="label-text">Remember me</span>
                    <input type="checkbox" name="remember" class="checkbox" />
                </label>
                <button on:click=move |_| {}> Login! </button>
            </ActionForm >
        </div>
    }
}

#[component]
fn Register(
    cx: Scope,
    register: Action<ucs_rpc::Register, Result<Option<String>, ServerFnError>>,
) -> impl IntoView {
    let (password, set_password) = create_signal(cx, String::new());
    let (confirm, set_confirm) = create_signal(cx, String::new());

    view! {
        cx,
        <div>
            <h2> Register</h2>
            <ActionForm class="form-control" action=register>
                <label class="label">
                    <span class="label-text">Username</span>
                </label>
                <input type="text" class="input input-bordered" name="username" />
                <label class="label">
                    <span class="label-text">Password</span>
                </label>
                <input type="password" class="input input-bordered" name="password" on:input=move |ev| set_password.set(event_target_value(&ev)) />
                <label class="label">
                    <span class="label-text">Password</span>
                </label>
                <input type="password" class="input input-bordered" name="password_confirm" on:input=move |ev| set_confirm.set(event_target_value(&ev)) />
                <label class="label" class:hidden={move || password.get() == confirm.get()}>
                    <span class="text-label text-red-500" >"Passwords do not match"</span>
                </label>
                <label class="label">
                    <span class="label-text">Invite Code</span>
                </label>
                <input type="text" class="input input-bordered" name="invite_code"/>

                <label class="label cursor-pointer">
                    <span class="label-text">Remember me</span>
                    <input type="checkbox" name="remember" class="checkbox" />
                </label>
                <button on:click=move |_| {}> Register! </button>
            </ActionForm >
        </div>
    }
}

#[component]
pub fn Auth(
    cx: Scope,
    login: Action<ucs_rpc::Login, Result<(), ServerFnError>>,
    register: Action<ucs_rpc::Register, Result<Option<String>, ServerFnError>>,
) -> impl IntoView {
    view! {cx,
        <div class="flex flex-row gap-5">

            <Login login=login/>
            <div class="divider divider-horizontal" >OR</div>
            <Register register=register/>
        </div>
    }
}
