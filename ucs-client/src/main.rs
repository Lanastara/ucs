use app::App;
use leptos::*;
use leptos_meta::{provide_meta_context, Stylesheet};
use leptos_router::Router;

mod app;
fn main() {
    console_error_panic_hook::set_once();

    tracing_wasm::set_as_global_default();

    mount_to_body(|cx| {
        provide_meta_context(cx);
        view! { cx,
          <Stylesheet id="leptos" href="style.css"/>
          <Router>
              <App />
          </Router>
        }
    })
}
