use leptos::*;
use leptos_icons::*;
use leptos_router::*;
use ucs_rpc::*;

use auth::Auth;

mod auth;

#[component]
pub fn App(cx: Scope) -> impl IntoView {
    let login = create_server_action::<Login>(cx);
    let register = create_server_action::<Register>(cx);
    let logout = create_server_action::<Logout>(cx);
    let location = use_location(cx);

    let user = create_resource(
        cx,
        move || {
            (
                login.version().get(),
                register.version().get(),
                logout.version().get(),
            )
        },
        move |_| get_user(cx),
    );

    view! {cx,
            <div class="drawer flex-1 flex flex-col" >
                <input type="checkbox" id="drawer" class="drawer-toggle" />

                <Routes>
                    <Route path="" view=move |cx | {
                            view!{cx, <Main user=user logout=logout />}
                        }
                    />
                    <Route path="/login" view=move |cx| view!{cx,
                        <div class="flex flex-col items-center">
                            <Auth login=login register=register />
                        </div>
                    } />
                </Routes>
            </div>
    }
}

#[component]
fn Main<S: PartialEq<S> + Clone + 'static>(
    cx: Scope,
    user: Resource<S, Result<Option<ucs_rpc::auth::User>, ServerFnError>>,
    logout: Action<Logout, Result<(), ServerFnError>>,
) -> impl IntoView {
    view! {cx,
        <div class="drawer-content">
            <header class="navbar bg-base-100 rounded-lg border shadow">
                <label for="drawer" class="btn"><Icon icon=icon!(ChMenuMeatball) /></label>
            </header>
            <div class="hero h-full">
                <div class="text-5xl font-bold">Universal Character Sheet</div>
            </div>
        </div>
        <div class="drawer-side">
            <label for="drawer" class="drawer-overlay"></label>
            <div class="bg-base-200 h-full flex flex-col">

            // Menu

            { move || {
                    user.read(cx).map(|user| match user{
                        Err(_) | Ok(None) => view!{cx, <div class="flex-1" /><A class="btn" href="/login" >Login</A>}.into_view(cx),
                        Ok(Some(_)) => view!{cx, <div class="flex-1" /><ActionForm action=logout><button class="btn">Logout</button></ActionForm>}.into_view(cx)
                    })
                }

            }
            </div>
        </div>
    }
}
