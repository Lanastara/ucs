use evalexpr::Node;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct System {
    pub attributes: BTreeMap<String, Attribute>,
    pub features: BTreeMap<String, Feature>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Attribute {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub formula: Option<Node>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Feature {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub condition: Option<Node>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub group: Option<String>,

    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub additional_features: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub effects: Vec<Effect>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Effect {
    #[serde(default)]
    pub priority: u8,
    pub formula: Node,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Character {
    pub attributes: BTreeMap<String, i64>,
    pub features: BTreeMap<String, String>,
    pub texts: BTreeMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Sheet {
    pages: Vec<Page>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Page {
    element: Size<Layout>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Size<T> {
    top: Option<Unit>,
    left: Option<Unit>,
    width: Option<Unit>,
    height: Option<Unit>,
    inner: T,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum Unit {
    Px(f32),
    Em(f32),
    Percent(f32),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum Layout {
    Grid(Grid),
    Column(RowCol),
    Row(RowCol),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Grid {
    cols: Vec<GridDef>,
    rows: Vec<GridDef>,
    elements: Vec<GridElement>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum GridDef {
    Auto,
    Relative(f32),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct GridElement {
    element: Size<Element>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum Element {
    Layout(Layout),
    Field(Field),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum Field {
    Attribute(String),
    Label(String),
    Feature(String),
    FeatureList(String),
    Text(String),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RowCol {
    elements: Vec<RowColElement>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RowColElement {
    def: RowColDef,
    element: Option<Element>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub enum RowColDef {
    Auto,
    Relative(f32),
}

#[cfg(test)]
mod test {

    use super::*;

    const CHARACTER: &str = include_str!("../../data/char.json");
    const SYSTEM: &str = include_str!("../../data/system.json");

    #[test]
    fn parse_character() {
        let _c: Character = serde_json::from_str(CHARACTER).unwrap();
    }

    #[test]
    fn parse_system() {
        let _s: System = serde_json::from_str(SYSTEM).unwrap();
    }
}
